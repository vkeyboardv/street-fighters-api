# street-fighters-API

Simple RESTful API using Node.js, Express, mongodb for stage-2-es6-for-everyone.

Heroku [https://street-fighters-api.herokuapp.com/api/fighters](https://street-fighters-api.herokuapp.com/api/fighters)

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install stage-2-express-yourself-with-nodejs.

```bash
npm i
```

## Usage

```bash
npm start
```
Go to [http://localhost:3001](http://localhost:3001)

## Requests
| HTTP Method  | Description |
| ------------- | ------------- |
| GET: /api/fighters  | Get list of all fighters  |
| GET: /details/fighter/:id  | Get fighter by id  |

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

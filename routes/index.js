var express = require('express');
var router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const uri = 'mongodb+srv://admin:admin@cluster0-2liir.mongodb.net/test?retryWrites=true';
const client = new MongoClient(uri, { useNewUrlParser: true });

const objectId = require('mongodb').ObjectID;

router.get('/api/fighters', (req, res, next) => {
  client.connect(err => {
    const collection = client.db('fighters').collection('fighters');
    collection.find({}).toArray((err, result) => {
      if (err) {
        res.send(err);
      } else {

        res.send(result);
      }
    })
  });
});

router.get('/api/details/fighter/:id', (req, res, next) => {
  const id = new objectId(req.params.id);

  client.connect(err => {
    const collection = client.db('fighters').collection('fighters');
    collection.findOne({ _id: id }, (err, result) => {
      if (err) {
        res.send(err);
      } else {

        res.send(result);
      }
    })
  });
});

module.exports = router;
